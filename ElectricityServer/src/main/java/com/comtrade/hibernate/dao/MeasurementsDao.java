package com.comtrade.hibernate.dao;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Component;

import com.comtrade.eletricity.model.Measurements;



@Component
public class MeasurementsDao implements MeasurementsDaoInterface<Measurements, String> {

	private Session currentSession;

	private Transaction currentTransaction;

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public void persist(Measurements entity) {
		getCurrentSession().save(entity);
	}

	public void update(Measurements entity) {
		getCurrentSession().update(entity);
	}

	public Measurements findById(String id) {
		Measurements measurement = (Measurements) getCurrentSession().get(Measurements.class, id);
		return measurement;
	}

	public void delete(Measurements entity) {
		getCurrentSession().delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<Measurements> findAllOrderedByMonthAndYear() {
		List<Measurements> measurements = (List<Measurements>) getCurrentSession()
				.createQuery("from Measurements ORDER BY year DESC, month DESC").list();

		for (int i = 1; i < measurements.size(); i++) {
			measurements.get(i - 1)
					.setMeasurement(measurements.get(i - 1).getMeasurement() - measurements.get(i).getMeasurement());
		}
		return measurements;
	}

	public Measurements findByDate(LocalDate date) {
		Measurements currentMeasurement = (Measurements) getCurrentSession()
				.createQuery(
						"from Measurements where month = " + date.getMonthValue() + " and year = " + date.getYear())
				.list().get(0);

		date = date.minusMonths(1);

		Measurements lastMeasurement = (Measurements) getCurrentSession()
				.createQuery(
						"from Measurements where month = " + date.getMonthValue() + " and year = " + date.getYear())
				.list().get(0);

		currentMeasurement.setMeasurement(currentMeasurement.getMeasurement() - lastMeasurement.getMeasurement());

		return currentMeasurement;
	}

	public void deleteAll() {
		List<Measurements> entityList = findAllOrderedByMonthAndYear();
		for (Measurements entity : entityList) {
			delete(entity);
		}
	}
}
