 package com.comtrade.hibernate.dao;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;


public interface MeasurementsDaoInterface<T, Id extends Serializable> {

	public void persist(T entity);
	
	public void update(T entity);
	
	public T findById(Id id);
	
	public void delete(T entity);
	
	public List<T> findAllOrderedByMonthAndYear();
	
	public T findByDate(LocalDate date);
	
	public void deleteAll();
	
}
