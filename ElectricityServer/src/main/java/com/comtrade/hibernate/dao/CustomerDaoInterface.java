package com.comtrade.hibernate.dao;

import java.util.List;

import com.comtrade.eletricity.model.Customer;

public interface CustomerDaoInterface {
	public List<Customer> findAll();
}
