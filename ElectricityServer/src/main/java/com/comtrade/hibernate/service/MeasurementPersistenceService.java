package com.comtrade.hibernate.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comtrade.eletricity.model.Measurements;
import com.comtrade.hibernate.dao.MeasurementsDao;

@Component
public class MeasurementPersistenceService {

	@Autowired
	private MeasurementsDao measurementsDao;

	public void persist(Measurements entity) {
		measurementsDao.openCurrentSessionwithTransaction();
		measurementsDao.persist(entity);
		measurementsDao.closeCurrentSessionwithTransaction();
	}

	public void update(Measurements entity) {
		measurementsDao.openCurrentSessionwithTransaction();
		measurementsDao.update(entity);
		measurementsDao.closeCurrentSessionwithTransaction();
	}

	public Measurements findById(String id) {
		measurementsDao.openCurrentSession();
		Measurements dbMeasurement = measurementsDao.findById(id);
		measurementsDao.closeCurrentSession();
		return dbMeasurement;
	}

	public void delete(String id) {
		measurementsDao.openCurrentSessionwithTransaction();
		Measurements dbMeasurement = measurementsDao.findById(id);
		measurementsDao.delete(dbMeasurement);
		measurementsDao.closeCurrentSessionwithTransaction();
	}

	public List<Measurements> findAllOrderedByMonthAndYear() {
		measurementsDao.openCurrentSession();
		List<Measurements> dbMeasurements = measurementsDao.findAllOrderedByMonthAndYear();
		measurementsDao.closeCurrentSession();
		return dbMeasurements;
	}

	public Measurements findByDate(LocalDate date) {
		measurementsDao.openCurrentSession();
		Measurements dbMeasurement = measurementsDao.findByDate(date);
		measurementsDao.closeCurrentSession();
		return dbMeasurement;
	}

	public void deleteAll() {
		measurementsDao.openCurrentSessionwithTransaction();
		measurementsDao.deleteAll();
		measurementsDao.closeCurrentSessionwithTransaction();
	}

	public MeasurementsDao measurementsDao() {
		return measurementsDao;
	}
}
