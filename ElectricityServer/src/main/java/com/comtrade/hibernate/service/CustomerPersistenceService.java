package com.comtrade.hibernate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comtrade.eletricity.model.Customer;
import com.comtrade.hibernate.dao.CustomerDao;

@Component
public class CustomerPersistenceService {

	@Autowired
	private CustomerDao customerDao;

	public List<Customer> findAll() {

		customerDao.openCurrentSession();
		List<Customer> customers = customerDao.findAll();
		customerDao.closeCurrentSession();
		
		return customers;
	}

}
