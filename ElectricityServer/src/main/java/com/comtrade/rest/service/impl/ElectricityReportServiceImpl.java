package com.comtrade.rest.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.springframework.beans.factory.annotation.Autowired;

import com.comtrade.eletricity.data.ElectricityData;
import com.comtrade.eletricity.data.MonthlyData;
import com.comtrade.eletricity.data.User;
import com.comtrade.eletricity.model.Measurements;
import com.comtrade.hibernate.service.MeasurementPersistenceService;
import com.comtrade.rest.service.ElectricityReportService;
import com.comtrade.utils.ReportUtil;

public class ElectricityReportServiceImpl implements ElectricityReportService {

	@Autowired
	private ReportUtil reportUtil;

	@Autowired
	private MeasurementPersistenceService measurementService;

	Random random = new Random();

	@Override
	public Response getReportMonthly(int monthNumber, int yearNumber) {

		byte[] data = null;

		try {

			ElectricityData elctricityData = new ElectricityData();
			elctricityData.setMonthlyData(createMonthlyData(monthNumber, yearNumber));
			elctricityData.setUser(createUser());

			final IRunAndRenderTask task = reportUtil.createRenderTask("month_report.rptdesign", elctricityData);

			task.run();
			task.close();

			data = reportUtil.readReportData(task);

		} catch (final EngineException e) {
			e.printStackTrace();
		}

		return Response.status(Status.OK).entity(data).build();

	}

	@Override
	public Response getReportYearly() {

		byte[] data = null;

		try {

			ElectricityData electricityData = new ElectricityData();
			electricityData.setYearlyData(createYearlyData());
			electricityData.setUser(createUser());

			final IRunAndRenderTask task = reportUtil.createRenderTask("year_report.rptdesign", electricityData);

			task.run();
			task.close();

			data = reportUtil.readReportData(task);

		} catch (final EngineException e) {
			e.printStackTrace();
		}

		return Response.status(Status.OK).entity(data).build();

	}

	private List<Measurements> createYearlyData() {

		List<Measurements> dbMeasurements = null;

		try {
			dbMeasurements = measurementService.findAllOrderedByMonthAndYear();

		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Measurements> tmpList = dbMeasurements.stream().limit(13).collect(Collectors.toList());

		Collections.reverse(tmpList);

		return tmpList;
	}

	private MonthlyData createMonthlyData(int monthNumber, int yearNumber) {

		Measurements currentMeasuremnt = null;
		Measurements lastMeasurement = null;

		LocalDate firstDay = LocalDate.now().withYear(yearNumber).withMonth(monthNumber).withDayOfMonth(1);
		LocalDate lastDay = firstDay.plusMonths(1).minusDays(1);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

		try {
			currentMeasuremnt = measurementService.findByDate(firstDay);
			lastMeasurement = measurementService.findByDate(firstDay.minusMonths(1));

		} catch (Exception e) {
			e.printStackTrace();
		}

		Optional<Measurements> currentOptional = Optional.ofNullable(currentMeasuremnt);
		Optional<Measurements> lastOptional = Optional.ofNullable(lastMeasurement);

		return new MonthlyData("" + random.nextInt(10000000),
				firstDay.format(formatter) + " - " + lastDay.format(formatter), LocalDate.now(), "Kragujevac",
				lastOptional.orElse(new Measurements()).getMeasurement(),
				currentOptional.orElse(new Measurements()).getMeasurement());

	}

	private User createUser() {
		User user = new User();
		user.setFirstName("Pera");
		user.setLastName("Peric");
		user.setID("" + random.nextInt(10000000));
		user.setAddress("Svetozara Markovica 1B/23, 34000 Kragujevac");
		user.setPhoneNumber("034/100-100");

		return user;
	}

}
