package com.comtrade.rest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public interface ElectricityReportService {

	@GET
	@Path("/month/{monthNumber}/{yearNumber}")
	@Produces(MediaType.TEXT_PLAIN)
	Response getReportMonthly(@PathParam("monthNumber") int monthNumber,@PathParam("yearNumber") int yearNumber);	

	@GET
	@Path("/year")
	@Produces(MediaType.TEXT_PLAIN)
	Response getReportYearly();

}