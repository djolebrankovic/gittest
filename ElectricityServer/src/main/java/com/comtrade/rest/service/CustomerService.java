package com.comtrade.rest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public interface CustomerService {

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	Response getAllUsers();

}