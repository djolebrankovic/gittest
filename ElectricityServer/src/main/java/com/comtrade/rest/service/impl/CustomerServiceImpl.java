package com.comtrade.rest.service.impl;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.cxf.jaxrs.impl.ResponseBuilderImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.comtrade.eletricity.model.Account;
import com.comtrade.eletricity.model.Customer;
import com.comtrade.hibernate.service.CustomerPersistenceService;
import com.comtrade.rest.service.CustomerService;

public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerPersistenceService customerPersistenceService;

	@Override
	public Response getAllUsers() {

		List<Customer> customers = customerPersistenceService.findAll();

		for (Customer customer : customers) {
			for (Account acount : customer.getAccounts()) {
				acount.setCustomer(null);
			}
		}

		return new ResponseBuilderImpl().status(Status.OK).entity(customers).build();
	}

}
