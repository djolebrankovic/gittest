package com.comtrade.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IPDFRenderOption;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.springframework.stereotype.Component;

import com.comtrade.eletricity.data.ElectricityData;
	
@Component("reportUtil")
public class ReportUtil {

	private EngineConfig config;
	private IReportEngine engine;

	@PostConstruct
	public void startReportEngine() {

		try {
			config = new EngineConfig();

			Platform.startup(config);
			final IReportEngineFactory factory = (IReportEngineFactory) Platform
					.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
			engine = factory.createReportEngine(config);
		} catch (final Exception exception) {
			exception.printStackTrace();
		}
	}

	public byte[] readReportData(final IRunAndRenderTask task) {
		try (final ByteArrayOutputStream outputStream = (ByteArrayOutputStream) task.getRenderOption()
				.getOutputStream()) {
			return outputStream.toByteArray();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public IRunAndRenderTask createRenderTask(final String filename, ElectricityData electricityData)
			throws EngineException {
		final IRunAndRenderTask task = engine.createRunAndRenderTask(loadReportDesign(filename));
		task.getAppContext().put("electricityData", electricityData);
		task.setRenderOption(createRenderOptions());

		return task;
	}

	private IReportRunnable loadReportDesign(final String filename) throws EngineException {

		try (final InputStream stream = new FileInputStream("reports/" + filename)) {
			return engine.openReportDesign(stream);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private PDFRenderOption createRenderOptions() {

		final PDFRenderOption options = new PDFRenderOption();
		options.setOutputFormat("pdf");
		options.setOption(IPDFRenderOption.DPI, new Integer(200));
		options.setOutputStream(new ByteArrayOutputStream());
		return options;
	}

}
