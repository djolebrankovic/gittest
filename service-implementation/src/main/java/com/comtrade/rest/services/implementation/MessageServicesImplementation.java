package com.comtrade.rest.services.implementation;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.comtrade.data.Message;
import com.comtrade.data.exception.CustomFileSystemException;
import com.comtrade.data.exception.CustomIOException;
import com.comtrade.rest.services.MessageServices;
import com.comtrade.sftp.client.SFTPClient;
import com.comtrade.utils.MessageUtils;

@Stateless
public class MessageServicesImplementation implements MessageServices {

	@EJB
	MessageUtils utils;

	@EJB
	SFTPClient sftpClient;

	private static final Logger logger = Logger.getLogger(SFTPClient.class.getName());

	public MessageServicesImplementation() {

	}

	public Response receiveMessage(Message message) {

		try {
			File localFile = utils.saveMessageToFile(message);

			sftpClient.readFileIfExists(message.getName());

			sftpClient.writeFile(localFile);

			return Response.ok(message.getText()).entity(message.toString()).build();
		} catch (CustomIOException e) {
			logger.log(Level.WARNING, e.getMessage(), e);
			return Response.serverError().entity(e.getMessage()).build();
		} catch (CustomFileSystemException e) {
			logger.log(Level.WARNING, e.getMessage(), e);
			return Response.serverError().entity(e.getMessage()).build();
		}

	}

}
