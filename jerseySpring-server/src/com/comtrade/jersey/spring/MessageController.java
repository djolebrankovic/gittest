package com.comtrade.jersey.spring;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;

import com.comtrade.data.Message;
import com.comtrade.data.exception.CustomIOException;
import com.comtrade.jersey.spring.base.BaseController;
import com.comtrade.utils.MessageUtils;

@Component
@Path("/message")
public class MessageController extends BaseController {

	private SpringBean springBean;

	@GET
	@Path("/hello_world")
	public Response getMessage() {

		springBean = (SpringBean) ContextLoader.getCurrentWebApplicationContext().getAutowireCapableBeanFactory()
				.getBean("springBean");
		String result = springBean.getValue();
		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/receive_message/{message}")
	public Response getMessage(@PathParam("message") String messageObj) {

		Message message = getGson().fromJson(messageObj, Message.class);

		MessageUtils utils = new MessageUtils();
		try {
			utils.saveMessageToFile(message);
			getLogger().info(message.getText());
		} catch (CustomIOException e) {
			getLogger().error(e.getMessage());
			e.printStackTrace();
		}

		return Response.status(200).entity(getGson().toJson(message)).build();
	}
}
