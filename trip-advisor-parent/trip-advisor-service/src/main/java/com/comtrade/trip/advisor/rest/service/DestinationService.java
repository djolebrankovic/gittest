package com.comtrade.trip.advisor.rest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/destination")
public interface DestinationService {

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	String getAllDestinations();
}
