package com.cotrade.trip.advisor.hibernate.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.comtrade.trip.advisor.model.Destination;

@Repository
public interface DestinationRepository extends CrudRepository<Destination, Long> {

}
