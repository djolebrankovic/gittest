package com.comtrade.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;

import org.apache.commons.io.IOUtils;
import org.codehaus.plexus.util.FileUtils;

import com.comtrade.data.Message;
import com.comtrade.data.exception.CustomIOException;

@Stateless
public class MessageUtils {

	private static final Logger logger = Logger.getLogger(MessageUtils.class.getName());

	public static final String LOCAL_PATH = "C:/temp/rest-test/";

	private BufferedReader bufferedReader;

	public File saveMessageToFile(Message message) throws CustomIOException {
		FileOutputStream outputStream = null;
		File file;

		// Create File With Relative Path
		// File dir = new File("tmp");
		// if (!dir.exists())
		// dir.mkdirs();
		//
		// file = new File(dir, message.getName() + ".txt");

		// Create file with apsolute Path

		File dir = new File(LOCAL_PATH);

		if (!dir.exists())
			dir.mkdirs();

		file = new File(dir, message.getName() + ".txt");

		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			outputStream = new FileOutputStream(file, true);

			byte[] contentBytes = message.getText().getBytes();
			outputStream.write(contentBytes);
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			throw new CustomIOException("Error write in file", e);
		} catch (NullPointerException e) {
			throw new CustomIOException("Null pointer exception");
		}

		logger.log(Level.INFO, "All data Successful save");

		return file;

	}

	public String readMessagesFromFile(Message message) {
		File file = new File(LOCAL_PATH + "/" + message.getName() + ".txt");

		try {
			bufferedReader = new BufferedReader(new FileReader(file));

			String text = "";
			String line = "";

			while ((line = bufferedReader.readLine()) != null)
				text += line;

			bufferedReader.close();
			return text;
		} catch (FileNotFoundException e) {
			logger.log(Level.WARNING, e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.log(Level.WARNING, e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public void copyTextBeetwenFile(File from, File to) throws FileNotFoundException, CustomIOException {

		FileInputStream inputStream = new FileInputStream(from);

		try {
			FileUtils.fileAppend(to.getAbsolutePath(), IOUtils.toString(inputStream));
			inputStream.close();
		} catch (IOException e) {
			throw new CustomIOException("Error coping file", e);
		}
	}
}
