package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EntityScan(basePackages = { "model" })
@EnableJpaRepositories(basePackages = { "repository" })
@ComponentScan(basePackages = "impl")
@EnableWebMvc
public class Application  {

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}

    
}
