package impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import model.Customer;
import repository.CustomerRepository;
import service.CustomerService;

@RestController
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository CustomerRepository;

	@Override
	public List<Customer> getAllCustomers() {
		return CustomerRepository.findAll();
	}

	@Override
	public String sayHello(String name) {
		return "Hello " + name;
	}

}
