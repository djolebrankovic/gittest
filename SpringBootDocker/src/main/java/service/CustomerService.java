package service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.Customer;

@RequestMapping("/djordje")
public interface CustomerService {

	@RequestMapping("/")
	List<Customer> getAllCustomers();

	@RequestMapping(value = { "/hello" }, method = { RequestMethod.POST })
	String sayHello(@RequestParam("personName") String personName);
	
}
