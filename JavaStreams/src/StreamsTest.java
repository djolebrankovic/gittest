import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsTest {

	public static void main(String[] args) {
		long count = 0;

		System.out.println("Filter:");

		List<Artist> allArtists = new ArrayList<>();
		allArtists.add(new Artist("Pera", "London"));
		allArtists.add(new Artist("Mika", "Paris"));
		allArtists.add(new Artist("Laza", "London"));

		Iterator<Artist> iterator = allArtists.iterator();
		while (iterator.hasNext()) {
			Artist artist = iterator.next();
			if (artist.isFrom("London")) {
				count++;
			}
		}

		System.out.println(count);
		count = 0;

		count = allArtists.stream().filter(a -> {
			System.out.println(a.getName());
			return a.isFrom("London");
		}).count();

		System.out.println(count);

		List<String> list = Stream.of("Pera", "Mika", "Laza").collect(Collectors.toList());
		list.size();

		System.out.println("Map:");

		List<String> collected = new ArrayList<>();
		for (String string : Arrays.asList("a", "b", "hello")) {
			String uppercaseString = string.toUpperCase();
			collected.add(uppercaseString);
		}

		collected.forEach(System.out::println);

		collected = Arrays.asList("a", "b", "hello").stream().map(s -> s.toUpperCase()).collect(Collectors.toList());

		collected.forEach(System.out::println);

		System.out.println("Flat map:");

		List<String> together = Stream.of(Arrays.asList("Pera", "Mika"), Arrays.asList("Laza", "Jana"))
				.flatMap(num -> num.stream()).collect(Collectors.toList());
		together.forEach(System.out::println);

		System.out.println("Comparing:");

		List<Truck> tracks = Arrays.asList(new Truck("Bakai", 524), new Truck("Violets for Your Furs", 378),
				new Truck("Time Was", 451));

		Truck track = tracks.stream().min(Comparator.comparing(t -> ((Truck) t).getSize())).get();

		System.out.println(track.getName() + " " + track.getSize());

		System.out.println("Reduce:");

		count = Stream.of(1, 2, 4).reduce(0, (acc, element) -> acc + element);

		System.out.println("Sum is: " + count);

		System.out.println("Multi foreach:");

		List<Album> albums = new ArrayList<>();

		Album album1 = new Album();
		List<Track> tracks1 = new ArrayList<>();

		tracks1.add(new Track("Track11", 100));
		tracks1.add(new Track("Track12", 50));
		tracks1.add(new Track("Track13", 150));

		album1.setTracks(tracks1);
		albums.add(album1);

		Album album2 = new Album();
		List<Track> tracks2 = new ArrayList<>();

		tracks2.add(new Track("Track21", 70));
		tracks2.add(new Track("Track22", 50));
		tracks2.add(new Track("Track23", 40));

		album2.setTracks(tracks2);
		albums.add(album2);

		List<String> trackNames = new ArrayList<>();

		albums.stream().forEach(album -> {
			album.getTracks().stream().filter(t -> ((Track) t).getLenght() > 60)
					.forEach(t -> trackNames.add(t.getName()));
		});

		trackNames.stream().forEach(System.out::println);

		count = albums.stream().flatMap(a -> a.getTracks().stream()).mapToInt(t -> t.getLenght()).sum();

		System.out.println(count);

		System.out.println("Stream with chars (finding keyword with longest lowercase)");

		List<String> keywords = new ArrayList<>();

		keywords.add("tesTtEstf");
		keywords.add("dasFasffaASd");
		keywords.add("dafpoFAAFS");

		Comparator<String> comparator = (a, b) -> {
			long lenA = a.chars().filter(c -> ((char) c) >= 'a' && ((char) c) <= 'z').count();
			long lenB = b.chars().filter(c -> ((char) c) >= 'a' && ((char) c) <= 'z').count();

			if (lenA > lenB)
				return 1;
			else if (lenA == lenB)
				return 0;
			else
				return -1;

		};

		String test = keywords.stream().max(comparator).get();
		System.out.println("max: " + test);

		System.out.println("Sorting ArrayList: ");

		keywords.stream().sorted(comparator).forEach(System.out::println);

		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("key1", "string");
		hashMap.put("key2", "stringg");
		hashMap.put("key3", "strin");
		hashMap.put("key4", "stringgg");

		hashMap.entrySet().stream().sorted(Map.Entry.comparingByValue(comparator)).forEach(System.out::println);

		System.out.println("List from HashMap with String contains:");

		keywords = hashMap.entrySet().stream().filter(m -> m.getValue().contains("gg")).map(m -> m.getValue())
				.collect(Collectors.toList());

		keywords.stream().forEach(System.out::println);

	}

}