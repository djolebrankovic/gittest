public class Artist {

	private String name;
	private String address;

	public Artist(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public boolean isFrom(String str) {
		return address.equals(str);
	}

	public String getName() {
		return name;
	}
}