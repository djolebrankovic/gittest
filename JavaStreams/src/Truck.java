public class Truck {

		private String name;
		private int size;

		public Truck(String name, int size) {
			this.name = name;
			this.size = size;
		}

		public int getSize() {
			return size;
		}

		public String getName() {
			return name;
		}

	}