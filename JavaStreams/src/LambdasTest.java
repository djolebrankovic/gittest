import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class LambdasTest {

	public static void main(String[] args) {

		MyComparator myComparator = (int a1, int a2) -> {
			if (a1 > a2)
				return "A is greather then B";
			else if (a1 < a2)
				return "A is less than B";
			else
				return "A is equals to B";
		};

		System.out.println(myComparator.compare(2, 1));

		List<Integer> numbers = new ArrayList<>();
		numbers.add(2);
		numbers.add(1);
		numbers.add(4);
		numbers.add(3);

		Comparator<Integer> comapartor = (Integer a, Integer b) -> {

			if (a > b)
				return 1;
			else if (a < b)
				return -1;
			else
				return 0;

		};

		numbers.sort(comapartor);

		numbers.stream().forEach(System.out::println);

	}

}
