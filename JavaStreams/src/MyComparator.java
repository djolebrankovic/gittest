public interface MyComparator {

	public String compare(int a1, int a2);

}