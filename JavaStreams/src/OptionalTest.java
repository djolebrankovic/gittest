import java.util.Optional;

public class OptionalTest {

	public static void main(String[] args) {

		Integer value1 = null;
		Integer value2 = new Integer(10);

		// Optional.ofNullable - allows passed parameter to be null.
		Optional<Integer> a = Optional.ofNullable(value1);

		// Optional.of - throws NullPointerException if passed parameter is null
		Optional<Integer> b = Optional.of(value2);
		System.out.println(sum(a, b));

		String str = "old";

		Optional<String> opt = Optional.ofNullable(str);

		System.out.println(opt.isPresent());
		System.out.println(opt.orElse("test"));

		System.out.println("Optional with lambdas");

		Optional<Car> carOptional = Optional.empty();
		carOptional.filter(x -> x.getPrice() == 250).ifPresent(x -> System.out.println(x.getPrice()));

		carOptional = Optional.of(new Car(200));
		carOptional.filter(x -> x.getPrice() == 250).ifPresent(x -> System.out.println(x.getPrice()));

		carOptional = Optional.of(new Car(250));
		carOptional.filter(x -> x.getPrice() == 250).ifPresent(x -> System.out.println("Found! " + x.getPrice()));

		Optional<String> strOptional = Optional.empty();
		Optional<Integer> sizeOptional = strOptional.map(String::length);

		System.out.println("Size of Text: " + sizeOptional.orElse(0));

		strOptional = Optional.of("test te st");
		sizeOptional = strOptional.map(String::length);

		System.out.println("Size of Text: " + sizeOptional.get());

	}

	static Integer sum(Optional<Integer> a, Optional<Integer> b) {

		// Optional.isPresent - checks the value is present or not

		System.out.println("First parameter is present: " + a.isPresent());
		System.out.println("Second parameter is present: " + b.isPresent());

		// Optional.orElse - returns the value if present otherwise returns
		// the default value passed.
		Integer value1 = a.orElse(new Integer(0));

		// Optional.get - gets the value, value should be present
		Integer value2 = b.get();
		return value1 + value2;
	}
}
