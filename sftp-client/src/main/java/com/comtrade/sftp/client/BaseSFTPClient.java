package com.comtrade.sftp.client;

import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;

public class BaseSFTPClient {

    private static final String IP = "10.113.80.5";
    // private static final Integer PORT = 22;
    private static final String USERNAME = "djordje";
    private static final String PASSWORD = "Djole034";
    private static final Integer TIMEOUT = 10000;

    private String uri;
    private FileSystemOptions opts;

    public BaseSFTPClient() {

    }

    public String getUri() {
        if (uri == null)
            buildUri();
        return uri;
    }

    private void buildUri() {
        StringBuilder sb = new StringBuilder();
        sb.append("sftp://").append(USERNAME).append(":").append(PASSWORD).append("@").append(IP)
                .append("/");
        uri = sb.toString();
    }

    public FileSystemOptions getOptions() throws FileSystemException {
        if (opts == null) {
            opts = new FileSystemOptions();
            SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");
            SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
            SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, TIMEOUT);
        }

        return opts;
    }
}
