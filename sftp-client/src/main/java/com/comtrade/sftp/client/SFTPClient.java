package com.comtrade.sftp.client;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;

import com.comtrade.data.exception.CustomFileSystemException;
import com.comtrade.data.exception.CustomIOException;
import com.comtrade.utils.MessageUtils;

@Stateless
public class SFTPClient extends BaseSFTPClient {

	private static final Logger logger = Logger.getLogger(SFTPClient.class.getName());

	public SFTPClient() {
		super();
	}

	public void writeFile(File file) throws CustomFileSystemException {

		StandardFileSystemManager manager = new StandardFileSystemManager();

		if (file == null || !file.exists())
			throw new RuntimeException("Local file not found");

		// Initializes the file manager

		try {
			manager.init();

			String sftpUri = getUri() + file.getName();

			// Create local file object
			FileObject localFile = manager.resolveFile(file.getAbsolutePath());

			// Create remote file object
			FileObject remoteFile = manager.resolveFile(sftpUri, getOptions());

			// Copy local file to sftp server
			remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);
			logger.log(Level.INFO, "File upload successful");
			localFile.delete();
			logger.log(Level.INFO, "Local File successful deleted");

		} catch (FileSystemException e) {
			throw new CustomFileSystemException(e);
		} finally {
			manager.close();
		}

	}

	public void readFileIfExists(String fileName) throws CustomFileSystemException, CustomIOException {

		StandardFileSystemManager manager = new StandardFileSystemManager();

		// Initializes the file manager
		try {
			manager.init();

			String sftpUri = getUri() + fileName + ".txt";

			// Create local file object
			FileObject localFile = manager.resolveFile(MessageUtils.LOCAL_PATH + fileName + ".txt");

			// Create remote file object
			FileObject remoteFile = manager.resolveFile(sftpUri, getOptions());

			if (remoteFile.exists()) {
				if (localFile.exists()) // If there is a local file, it means
										// that it is not sent to the server
				{
					FileObject tempFile = manager.resolveFile(MessageUtils.LOCAL_PATH + "tmp.txt");

					tempFile.copyFrom(remoteFile, Selectors.SELECT_SELF);

					MessageUtils utils = new MessageUtils();

					File local = new File(MessageUtils.LOCAL_PATH + fileName + ".txt");
					File temp = new File(MessageUtils.LOCAL_PATH + "tmp.txt");

					try {
						utils.copyTextBeetwenFile(local, temp);
					} catch (FileNotFoundException e) {
						throw new CustomIOException("File not found", e);
					} catch (IOException e) {
						throw new CustomIOException(e);
					}

					local.delete();
					temp.renameTo(local);

				} else {
					localFile.copyFrom(remoteFile, Selectors.SELECT_SELF);
				}

			}
		} catch (FileSystemException e1) {
			throw new CustomFileSystemException(e1);
		}

		logger.log(Level.INFO, "File read successful");

		manager.close();

	}

	public boolean isFileExists(String fileName) {

		StandardFileSystemManager manager = new StandardFileSystemManager();

		try {

			// Initializes the file manager
			manager.init();

			String sftpUri = getUri() + fileName + ".txt";

			// Create remote file object
			FileObject remoteFile = manager.resolveFile(sftpUri, getOptions());

			return (remoteFile.exists());

		} catch (FileSystemException ex) {
			ex.printStackTrace();
			logger.log(Level.WARNING, "Error access in file", ex);

		} finally {
			manager.close();
		}
		return false;
	}
}
