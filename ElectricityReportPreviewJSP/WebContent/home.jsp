<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.eclipse.org/birt/taglibs/birt.tld"
	prefix="birt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Electricity Reports</title>
</head>



<body>

	<form action="initialize">
			Korisnik:
			<select name="customer" onchange="this.form.submit()" >
				<c:forEach var="customerElem" items="${customers}">
					<option value="${ customerElem.id}"  ${customerElem.id == selectedCustomer ? 'selected="selected"' : ''}>${customerElem.firstName }</option>
				</c:forEach>
			</select> 
			
			Nalog:
			<select name="account" onchange="myFunction()" id="populateDropDown">
				<c:forEach var="accountElem" items="${accounts}" >
					<option value="${ accountElem.id}">${accountElem.accountNumber }</option>
				</c:forEach>
			</select>
	</form>

	<div align="center" style="margin-top: 50px;">
		<form action="ReportAction"
			style="text-align: left; border: 1px solid black">
			

			<div style="font-size: 30px">Mesecni Izvestaj</div>
			<br /> 
			Unesite Mesec: 
			<select name="month" >
				<option value="1">Januar</option>
				<option value="2">Februar</option>
				<option value="3">Mart</option>
				<option value="4">April</option>
				<option value="5">Maj</option>
				<option value="6">Jun</option>
				<option value="7">Jul</option>
				<option value="8">Avgust</option>
				<option value="9">Septembar</option>
				<option value="10">Oktobar</option>
				<option value="11">Novembar</option>
				<option value="12">Decembar</option>
			</select> 
			<br /> Unesite Godinu: <input type="number" name="year" value="2015" min="2015" max="2020"><br />
			<input type="submit" value="Monthly Report" name="MonthlyReport"
				style="margin-left: 100px">
				
			<input type="hidden" name="accountId" id="accountId" value = <c:out value="${accountId}"/>
		</form>
				
		<form action="ReportAction"
			style="text-align: left; border: 1px solid black">
			<div style="font-size: 30px">Godisnji Izvestaj</div>
			<br /> 
			<input type="submit" value="Yearly Report"
				name="YearlyReport" style="margin-left: 100px">
		</form>
	</div>
<script type="text/javascript">
	function populateDropDown() {
		
		var e = document.getElementById("accountDropDown");
		var accountId = e.options[e.selectedIndex].value;
					
		document.getElementById("accountId").value = accountId; 
	}
</script>
	
</html>