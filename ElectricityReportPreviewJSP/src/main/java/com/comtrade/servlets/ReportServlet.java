package com.comtrade.servlets;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;

import com.comtrade.rest.client.RESTClient;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/ReportServlet")
public class ReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String MONTH = "month";
	private static final String YEAR = "year";
	private static final String MONTH_PATH = "/services/report/month";
	private static final String YEAR_PATH = "/services/report/year";

	private static final String MONTHLY_REPORT = "MonthlyReport";
	private static final String YEARLY_REPORT = "YearlyReport";
	private RESTClient restClient;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportServlet() {
		super();
		restClient = new RESTClient();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if (request.getParameter(MONTHLY_REPORT) != null) {

			if (validate(request, response)) {

				StringBuilder stringBuilder = new StringBuilder();

				stringBuilder.append(MONTH_PATH).append("/").append(request.getParameter(MONTH)).append("/")
						.append(request.getParameter(YEAR));

				Response clientResponse = restClient.get(stringBuilder.toString());
				InputStream is = clientResponse.readEntity(InputStream.class);
				byte[] bytes = IOUtils.toByteArray(is);
				response.getOutputStream().write(bytes);
			}

		} else if (request.getParameter(YEARLY_REPORT) != null) {

			Response clientResponse = restClient.get(YEAR_PATH);
			InputStream is = clientResponse.readEntity(InputStream.class);
			byte[] bytes = IOUtils.toByteArray(is);
			response.getOutputStream().write(bytes);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private boolean validate(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String month = request.getParameter(MONTH);
		String year = request.getParameter(YEAR);

		if (month == null || month.equals("")) {
			response.getOutputStream().write("Value for month may not be empty".getBytes());
			return false;
		}

		if (year == null || year.equals("")) {
			response.getOutputStream().write("Value for year may not be empty".getBytes());
			return false;
		}

		try {
			new Integer(year);

		} catch (Exception e) {
			response.getOutputStream().write("Value for year must be a number.".getBytes());
			return false;
		}

		return true;
	}

}
