package com.comtrade.servlets;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import com.comtrade.eletricity.model.Customer;
import com.comtrade.rest.client.RESTClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Servlet implementation class Redirect
 */
@WebServlet("/initialize")
public class Redirect extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String USER_PATH = "/services/customer/list";

	private RESTClient restClient;
	private Gson gson;
	private List<Customer> customers;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Redirect() {
		super();

		restClient = new RESTClient();

		gson = new Gson();

		Response clientResponse = restClient.get(USER_PATH);

		Type type = new TypeToken<List<Customer>>() {
		}.getType();

		customers = gson.fromJson(clientResponse.readEntity(String.class), type);

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameter("customer") == null) {

			request.setAttribute("customers", customers);
			request.setAttribute("accounts", customers.get(0).getAccounts());
			request.setAttribute("accountId", customers.get(0).getAccounts().get(0).getId());
			request.setAttribute("selectedCustomer", customers.get(0).getId());

		} else {
			int customerId = new Integer(request.getParameter("customer"));
			Customer customer = customers.stream().filter(c -> c.getId() == customerId).findFirst().get();

			request.setAttribute("accounts", customer.getAccounts());
			request.setAttribute("accountId",customer.getAccounts().get(0).getId());
			request.setAttribute("selectedCustomer", customer.getId());
			request.setAttribute("customers", customers);
		}

		request.getRequestDispatcher("home.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
