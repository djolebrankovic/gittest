package com.comtrade.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class RESTClient {
	private static final String HOST = "localhost";
	private static final int PORT = 8080;
	private Client client;
	private String basePath;

	public RESTClient() {
		final ClientBuilder builder = ClientBuilder.newBuilder();
		client = builder.build().register(new JacksonJsonProvider());
	}

	public synchronized Response get(String path) {
		return client.target(getBasePath() + path).request().get();
	}

	private String getBasePath() {
		if (basePath == null) {
			StringBuilder builder = new StringBuilder();
			builder.append("http://").append(HOST).append(":").append(PORT);
			basePath = builder.toString();
		}

		return basePath;
	}

}
