package com.comtrade.rest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("heartbeat")
public interface HeartbeatService {
	
	@GET
	@Path("/")
	public Response getHeartbeat();

}
