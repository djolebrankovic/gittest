package com.comtrade.rest.service.impl;

import javax.ws.rs.core.Response;

import com.comtrade.rest.service.HeartbeatService;

public class HeartbeatServiceImpl implements HeartbeatService {

	@Override
	public Response getHeartbeat() {
		return Response.ok().entity("Hello world").build();
	}

}
