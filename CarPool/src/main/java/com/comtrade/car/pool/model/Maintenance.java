package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the maintenance database table.
 * 
 */
@Entity
@NamedQuery(name="Maintenance.findAll", query="SELECT m FROM Maintenance m")
public class Maintenance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private String description;

	private String type;

	//bi-directional many-to-one association to Event
	@ManyToOne
	private Event event;

	public Maintenance() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

}