package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private String address;

	private String email;

	@Column(name="first_name")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	@Column(name="middle_name")
	private String middleName;

	private Integer oid;

	@Column(name="phone_number")
	private String phoneNumber;

	private String position;

	//bi-directional many-to-one association to AuthorizationOfUse
	@OneToMany(mappedBy="user")
	private List<AuthorizationOfUse> authorizationOfUses;

	//bi-directional many-to-one association to Car
	@OneToMany(mappedBy="user")
	private List<Car> cars;

	//bi-directional many-to-one association to Event
	@OneToMany(mappedBy="user")
	private List<Event> events;

	//bi-directional many-to-one association to Note
	@OneToMany(mappedBy="user")
	private List<Note> notes;

	//bi-directional many-to-many association to TravelWarrant
	@ManyToMany
	@JoinTable(
		name="passenger_travel_warrant"
		, joinColumns={
			@JoinColumn(name="user_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="travel_warrant_id")
			}
		)
	private List<TravelWarrant> travelWarrants;

	//bi-directional many-to-one association to Car
	@ManyToOne
	private Car car;

	//bi-directional many-to-one association to Department
	@ManyToOne
	private Department department;

	//bi-directional many-to-one association to Location
	@ManyToOne
	private Location location;

	//bi-directional many-to-many association to Group
	@ManyToMany
	@JoinTable(
		name="user_group"
		, joinColumns={
			@JoinColumn(name="user_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="group_id")
			}
		)
	private List<Group> groups;

	//bi-directional many-to-many association to Project
	@ManyToMany
	@JoinTable(
		name="user_project"
		, joinColumns={
			@JoinColumn(name="user_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="project_id")
			}
		)
	private List<Project> projects;

	public User() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public Integer getOid() {
		return this.oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public List<AuthorizationOfUse> getAuthorizationOfUses() {
		return this.authorizationOfUses;
	}

	public void setAuthorizationOfUses(List<AuthorizationOfUse> authorizationOfUses) {
		this.authorizationOfUses = authorizationOfUses;
	}

	public AuthorizationOfUse addAuthorizationOfUs(AuthorizationOfUse authorizationOfUs) {
		getAuthorizationOfUses().add(authorizationOfUs);
		authorizationOfUs.setUser(this);

		return authorizationOfUs;
	}

	public AuthorizationOfUse removeAuthorizationOfUs(AuthorizationOfUse authorizationOfUs) {
		getAuthorizationOfUses().remove(authorizationOfUs);
		authorizationOfUs.setUser(null);

		return authorizationOfUs;
	}

	public List<Car> getCars() {
		return this.cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public Car addCar(Car car) {
		getCars().add(car);
		car.setUser(this);

		return car;
	}

	public Car removeCar(Car car) {
		getCars().remove(car);
		car.setUser(null);

		return car;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setUser(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setUser(null);

		return event;
	}

	public List<Note> getNotes() {
		return this.notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Note addNote(Note note) {
		getNotes().add(note);
		note.setUser(this);

		return note;
	}

	public Note removeNote(Note note) {
		getNotes().remove(note);
		note.setUser(null);

		return note;
	}

	public List<TravelWarrant> getTravelWarrants() {
		return this.travelWarrants;
	}

	public void setTravelWarrants(List<TravelWarrant> travelWarrants) {
		this.travelWarrants = travelWarrants;
	}

	public Car getCar() {
		return this.car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Group> getGroups() {
		return this.groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

}