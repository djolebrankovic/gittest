package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the car database table.
 * 
 */
@Entity
@NamedQuery(name="Car.findAll", query="SELECT c FROM Car c")
public class Car implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name="car_type")
	private String carType;

	@Column(name="current_km")
	private Integer currentKm;

	@Column(name="engine_kilowatts")
	private Integer engineKilowatts;

	@Column(name="engine_size")
	private Integer engineSize;

	@Column(name="fuel_type")
	private String fuelType;

	private String name;

	@Column(name="number_of_doors")
	private Integer numberOfDoors;

	@Column(name="passanger_seat")
	private Integer passangerSeat;

	private String vin;

	//bi-directional many-to-one association to AuthorizationOfUse
	@OneToMany(mappedBy="car")
	private List<AuthorizationOfUse> authorizationOfUses;

	//bi-directional many-to-one association to Location
	@ManyToOne(fetch=FetchType.LAZY)
	private Location location;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	private User user;

	//bi-directional many-to-many association to Department
	@ManyToMany(mappedBy="cars")
	private List<Department> departments;

	//bi-directional many-to-one association to Event
	@OneToMany(mappedBy="car")
	private List<Event> events;

	//bi-directional many-to-one association to Image
	@OneToMany(mappedBy="car")
	private List<Image> images;

	//bi-directional many-to-one association to Note
	@OneToMany(mappedBy="car")
	private List<Note> notes;

	//bi-directional many-to-one association to Registration
	@OneToMany(mappedBy="car")
	private List<Registration> registrations;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="car")
	private List<User> users;

	public Car() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCarType() {
		return this.carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public Integer getCurrentKm() {
		return this.currentKm;
	}

	public void setCurrentKm(Integer currentKm) {
		this.currentKm = currentKm;
	}

	public Integer getEngineKilowatts() {
		return this.engineKilowatts;
	}

	public void setEngineKilowatts(Integer engineKilowatts) {
		this.engineKilowatts = engineKilowatts;
	}

	public Integer getEngineSize() {
		return this.engineSize;
	}

	public void setEngineSize(Integer engineSize) {
		this.engineSize = engineSize;
	}

	public String getFuelType() {
		return this.fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumberOfDoors() {
		return this.numberOfDoors;
	}

	public void setNumberOfDoors(Integer numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}

	public Integer getPassangerSeat() {
		return this.passangerSeat;
	}

	public void setPassangerSeat(Integer passangerSeat) {
		this.passangerSeat = passangerSeat;
	}

	public String getVin() {
		return this.vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public List<AuthorizationOfUse> getAuthorizationOfUses() {
		return this.authorizationOfUses;
	}

	public void setAuthorizationOfUses(List<AuthorizationOfUse> authorizationOfUses) {
		this.authorizationOfUses = authorizationOfUses;
	}

	public AuthorizationOfUse addAuthorizationOfUs(AuthorizationOfUse authorizationOfUs) {
		getAuthorizationOfUses().add(authorizationOfUs);
		authorizationOfUs.setCar(this);

		return authorizationOfUs;
	}

	public AuthorizationOfUse removeAuthorizationOfUs(AuthorizationOfUse authorizationOfUs) {
		getAuthorizationOfUses().remove(authorizationOfUs);
		authorizationOfUs.setCar(null);

		return authorizationOfUs;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Department> getDepartments() {
		return this.departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setCar(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setCar(null);

		return event;
	}

	public List<Image> getImages() {
		return this.images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Image addImage(Image image) {
		getImages().add(image);
		image.setCar(this);

		return image;
	}

	public Image removeImage(Image image) {
		getImages().remove(image);
		image.setCar(null);

		return image;
	}

	public List<Note> getNotes() {
		return this.notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Note addNote(Note note) {
		getNotes().add(note);
		note.setCar(this);

		return note;
	}

	public Note removeNote(Note note) {
		getNotes().remove(note);
		note.setCar(null);

		return note;
	}

	public List<Registration> getRegistrations() {
		return this.registrations;
	}

	public void setRegistrations(List<Registration> registrations) {
		this.registrations = registrations;
	}

	public Registration addRegistration(Registration registration) {
		getRegistrations().add(registration);
		registration.setCar(this);

		return registration;
	}

	public Registration removeRegistration(Registration registration) {
		getRegistrations().remove(registration);
		registration.setCar(null);

		return registration;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setCar(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setCar(null);

		return user;
	}

}