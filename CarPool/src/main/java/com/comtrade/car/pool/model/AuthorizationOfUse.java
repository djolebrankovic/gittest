package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the authorization_of_use database table.
 * 
 */
@Entity
@Table(name="authorization_of_use")
@NamedQuery(name="AuthorizationOfUse.findAll", query="SELECT a FROM AuthorizationOfUse a")
public class AuthorizationOfUse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Integer id;

	private byte[] document;

	private Boolean revoked;

	@Column(name="valid_from")
	private Timestamp validFrom;

	@Column(name="valid_until")
	private Timestamp validUntil;

	//bi-directional many-to-one association to Car
	@ManyToOne
	private Car car;

	//bi-directional many-to-one association to User
	@ManyToOne
	private User user;

	public AuthorizationOfUse() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getDocument() {
		return this.document;
	}

	public void setDocument(byte[] document) {
		this.document = document;
	}

	public Boolean getRevoked() {
		return this.revoked;
	}

	public void setRevoked(Boolean revoked) {
		this.revoked = revoked;
	}

	public Timestamp getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Timestamp validFrom) {
		this.validFrom = validFrom;
	}

	public Timestamp getValidUntil() {
		return this.validUntil;
	}

	public void setValidUntil(Timestamp validUntil) {
		this.validUntil = validUntil;
	}

	public Car getCar() {
		return this.car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}