package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the travel_warrant database table.
 * 
 */
@Entity
@Table(name="travel_warrant")
@NamedQuery(name="TravelWarrant.findAll", query="SELECT t FROM TravelWarrant t")
public class TravelWarrant implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name="additional_passengers_allowed")
	private Boolean additionalPassengersAllowed;

	private String category;

	@Column(name="created_date")
	private Timestamp createdDate;

	private String description;

	@Column(name="has_authorisation")
	private Boolean hasAuthorisation;

	private String location;

	private String type;

	//bi-directional many-to-one association to Note
	@OneToMany(mappedBy="travelWarrant")
	private List<Note> notes;

	//bi-directional many-to-many association to User
	@ManyToMany(mappedBy="travelWarrants")
	private List<User> users;

	//bi-directional many-to-one association to Event
	@ManyToOne
	private Event event;

	public TravelWarrant() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAdditionalPassengersAllowed() {
		return this.additionalPassengersAllowed;
	}

	public void setAdditionalPassengersAllowed(Boolean additionalPassengersAllowed) {
		this.additionalPassengersAllowed = additionalPassengersAllowed;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getHasAuthorisation() {
		return this.hasAuthorisation;
	}

	public void setHasAuthorisation(Boolean hasAuthorisation) {
		this.hasAuthorisation = hasAuthorisation;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Note> getNotes() {
		return this.notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Note addNote(Note note) {
		getNotes().add(note);
		note.setTravelWarrant(this);

		return note;
	}

	public Note removeNote(Note note) {
		getNotes().remove(note);
		note.setTravelWarrant(null);

		return note;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Event getEvent() {
		return this.event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

}