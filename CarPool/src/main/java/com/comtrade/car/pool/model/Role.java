package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator="seqGen")
	@SequenceGenerator(name="seqGen",sequenceName="role_id_seq", allocationSize=1)
	private Integer id;

	private String description;

	private String name;

	// bi-directional many-to-many association to Group
	@ManyToMany(mappedBy = "roles")
	private List<Group> groups;

	// bi-directional many-to-many association to Permission
	@ManyToMany(mappedBy = "roles")
	private List<Permission> permissions;

	public Role() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Group> getGroups() {
		return this.groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Permission> getPermissions() {
		return this.permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

}