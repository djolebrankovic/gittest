package com.comtrade.car.pool.application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.comtrade.rest.service.impl.HeartbeatServiceImpl;

@ApplicationPath("/")
public class RESTApplication extends Application {

	private Set<Object> singletons = new HashSet<>();
	private Set<Class<?>> empty = new HashSet<>();

	public RESTApplication() {
		singletons.add(new HeartbeatServiceImpl());
	}

	@Override
	public Set<Class<?>> getClasses() {
		return empty;
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

}
