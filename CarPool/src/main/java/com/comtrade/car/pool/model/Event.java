package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the event database table.
 * 
 */
@Entity
@NamedQuery(name="Event.findAll", query="SELECT e FROM Event e")
public class Event implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name="end_date")
	private Timestamp endDate;

	@Column(name="kilometers_completed")
	private Integer kilometersCompleted;

	@Column(name="start_date")
	private Timestamp startDate;

	@Column(name="start_kilometers")
	private Integer startKilometers;

	private String status;

	//bi-directional many-to-one association to Car
	@ManyToOne
	private Car car;

	//bi-directional many-to-one association to EventRecurrence
	@ManyToOne
	@JoinColumn(name="event_recurrence_id")
	private EventRecurrence eventRecurrence;

	//bi-directional many-to-one association to User
	@ManyToOne
	private User user;

	//bi-directional many-to-one association to Maintenance
	@OneToMany(mappedBy="event")
	private List<Maintenance> maintenances;

	//bi-directional many-to-one association to Note
	@OneToMany(mappedBy="event")
	private List<Note> notes;

	//bi-directional many-to-one association to TravelWarrant
	@OneToMany(mappedBy="event")
	private List<TravelWarrant> travelWarrants;

	public Event() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Integer getKilometersCompleted() {
		return this.kilometersCompleted;
	}

	public void setKilometersCompleted(Integer kilometersCompleted) {
		this.kilometersCompleted = kilometersCompleted;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Integer getStartKilometers() {
		return this.startKilometers;
	}

	public void setStartKilometers(Integer startKilometers) {
		this.startKilometers = startKilometers;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Car getCar() {
		return this.car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public EventRecurrence getEventRecurrence() {
		return this.eventRecurrence;
	}

	public void setEventRecurrence(EventRecurrence eventRecurrence) {
		this.eventRecurrence = eventRecurrence;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Maintenance> getMaintenances() {
		return this.maintenances;
	}

	public void setMaintenances(List<Maintenance> maintenances) {
		this.maintenances = maintenances;
	}

	public Maintenance addMaintenance(Maintenance maintenance) {
		getMaintenances().add(maintenance);
		maintenance.setEvent(this);

		return maintenance;
	}

	public Maintenance removeMaintenance(Maintenance maintenance) {
		getMaintenances().remove(maintenance);
		maintenance.setEvent(null);

		return maintenance;
	}

	public List<Note> getNotes() {
		return this.notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Note addNote(Note note) {
		getNotes().add(note);
		note.setEvent(this);

		return note;
	}

	public Note removeNote(Note note) {
		getNotes().remove(note);
		note.setEvent(null);

		return note;
	}

	public List<TravelWarrant> getTravelWarrants() {
		return this.travelWarrants;
	}

	public void setTravelWarrants(List<TravelWarrant> travelWarrants) {
		this.travelWarrants = travelWarrants;
	}

	public TravelWarrant addTravelWarrant(TravelWarrant travelWarrant) {
		getTravelWarrants().add(travelWarrant);
		travelWarrant.setEvent(this);

		return travelWarrant;
	}

	public TravelWarrant removeTravelWarrant(TravelWarrant travelWarrant) {
		getTravelWarrants().remove(travelWarrant);
		travelWarrant.setEvent(null);

		return travelWarrant;
	}

}