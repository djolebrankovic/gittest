package com.comtrade.car.pool.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the event_recurrence database table.
 * 
 */
@Entity
@Table(name="event_recurrence")
@NamedQuery(name="EventRecurrence.findAll", query="SELECT e FROM EventRecurrence e")
public class EventRecurrence implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private Boolean active;

	private Integer counter;

	@Column(name="recurrence_date")
	private String recurrenceDate;

	@Column(name="recurrence_kilometers")
	private Integer recurrenceKilometers;

	private Integer times;

	@Column(name="valid_until")
	private Timestamp validUntil;

	//bi-directional many-to-one association to Event
	@OneToMany(mappedBy="eventRecurrence")
	private List<Event> events;

	public EventRecurrence() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getCounter() {
		return this.counter;
	}

	public void setCounter(Integer counter) {
		this.counter = counter;
	}

	public String getRecurrenceDate() {
		return this.recurrenceDate;
	}

	public void setRecurrenceDate(String recurrenceDate) {
		this.recurrenceDate = recurrenceDate;
	}

	public Integer getRecurrenceKilometers() {
		return this.recurrenceKilometers;
	}

	public void setRecurrenceKilometers(Integer recurrenceKilometers) {
		this.recurrenceKilometers = recurrenceKilometers;
	}

	public Integer getTimes() {
		return this.times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public Timestamp getValidUntil() {
		return this.validUntil;
	}

	public void setValidUntil(Timestamp validUntil) {
		this.validUntil = validUntil;
	}

	public List<Event> getEvents() {
		return this.events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public Event addEvent(Event event) {
		getEvents().add(event);
		event.setEventRecurrence(this);

		return event;
	}

	public Event removeEvent(Event event) {
		getEvents().remove(event);
		event.setEventRecurrence(null);

		return event;
	}

}