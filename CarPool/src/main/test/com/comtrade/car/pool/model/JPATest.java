package com.comtrade.car.pool.model;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.comtrade.car.pool.model.Permission;
import com.comtrade.car.pool.model.Role;

public class JPATest {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CarPool");

		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		@SuppressWarnings("unchecked")
		List<Role> result = entityManager.createNamedQuery("Role.findAll").getResultList();

		for (Role role : result) {
			System.out.println(role.getName());
			for (Permission permission : role.getPermissions()) {
				System.out.print(permission.getName() + " ");
			}
			System.out.println();
		}

		Role role = new Role();
		role.setName("Super User");
		//entityManager.persist(role);
		entityManager.getTransaction().commit();
	
		
		System.exit(0);
	}

}
