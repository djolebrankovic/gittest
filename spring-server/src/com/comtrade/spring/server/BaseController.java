package com.comtrade.spring.server;

import org.slf4j.LoggerFactory;

import com.comtrade.utils.MessageUtils;
import com.google.gson.Gson;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;

public class BaseController {

	private Gson gson;
	private MessageUtils messageUtils;
	private Logger logger;

	public BaseController() {
		if (gson == null)
			gson = new Gson();

		if (messageUtils == null)
			messageUtils = new MessageUtils();

		if (logger == null)
			initLoger();
	}

	private void initLoger() {

		// // with set configurations
		// LoggerContext loggerContext = new LoggerContext();
		//
		// FileAppender<ILoggingEvent> fileAppender = new
		// FileAppender<ILoggingEvent>();
		// fileAppender.setContext(loggerContext);
		// fileAppender.setName("Controler");
		//
		// // set the file name
		// fileAppender.setFile("d:\\project\\gittest\\spring-server\\logs\\logs.log");
		//
		// PatternLayoutEncoder encoder = new PatternLayoutEncoder();
		// encoder.setContext(loggerContext);
		// encoder.setPattern("%-30([%date{dd.MM.yyyy HH:mm:ss.SSS}]) %-5level
		// %class => %msg%n" );
		// encoder.start();
		//
		// fileAppender.setEncoder(encoder);
		// fileAppender.start();
		//
		// // attach the rolling file appender to the logger of your choice
		// logger = loggerContext.getLogger("Main");
		// logger.addAppender(fileAppender);
		//
		// // OPTIONAL: print logback internal status messages
		// StatusPrinter.print(loggerContext);

		// with configurations from external file
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

		// try {
		// JoranConfigurator configurator = new JoranConfigurator();
		// configurator.setContext(context);
		// // Call context.reset() to clear any previous configuration, e.g.
		// // default
		// // configuration. For multi-step configuration, omit calling
		// // context.reset().
		// context.reset();
		// configurator.doConfigure("d:\\project\\gittest\\data\\config\\logback.xml");
		// } catch (JoranException je) {
		// // StatusPrinter will handle this
		// }
		StatusPrinter.printInCaseOfErrorsOrWarnings(context);

		logger = context.getLogger("Main");
		// logger.addAppender(fileAppender);

	}

	public Gson getGson() {
		return gson;
	}

	public MessageUtils getMessageUtils() {
		return messageUtils;
	}

	public Logger getLogger() {
		return logger;
	}

}
