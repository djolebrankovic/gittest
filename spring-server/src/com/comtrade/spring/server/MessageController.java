package com.comtrade.spring.server;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.comtrade.data.Message;
import com.comtrade.data.exception.CustomIOException;
import com.comtrade.spring.server.BaseController;

@Controller
@RequestMapping("/message")
public class MessageController extends BaseController {

	// Receive text with param
	@RequestMapping(value = "/receive_message", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String receiveMessage(HttpServletRequest request,
			@RequestParam(value = "message", required = false) String reqMessage) {

		Message message = getGson().fromJson(reqMessage, Message.class);

		getLogger().info("Receive message");

		try {
			getMessageUtils().saveMessageToFile(message);
		} catch (CustomIOException e) {
			e.printStackTrace();
		}

		return getGson().toJson(message.getName());
	}

	@RequestMapping(value = "/list_message", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String listMessage(HttpServletRequest request,
			@RequestParam(value = "message", required = false) String reqMessage) {

		Message message = getGson().fromJson(reqMessage, Message.class);

		String text = getMessageUtils().readMessagesFromFile(message);

		// System.out.println(text);
		getLogger().info(text);

		return getGson().toJson(getMessageUtils().readMessagesFromFile(message));
	}

	@RequestMapping(method = RequestMethod.POST)
	public Response printHelloPost(ModelMap model) {

		System.out.println("Method POST");

		return Response.ok("Method POST").build();
	}

}