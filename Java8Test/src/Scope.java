public class Scope {
	public static void main(String[] args) {
		String one, two;
		one = new String("a");
		two = new String("b");
		one = null;

		two = one;
		String three = two;

		Short s = 10;
		System.out.println("one: " + one == null);
		System.out.println("two: " + two == null);
		System.out.println("three: " + three == null);
	}
}