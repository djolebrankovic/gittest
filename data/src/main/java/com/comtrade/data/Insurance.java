package com.comtrade.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Insurance {

    private SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");

    private String insuranceNum;
    private Date validDate;

    public Insurance() {

    }

    public Insurance(String insuranceNum, Date validDate) {
        super();
        this.validDate = validDate;
        this.insuranceNum = insuranceNum;
    }

    public Insurance(String insuranceNum, String validDate) {
        super();
        this.insuranceNum = insuranceNum;
        try {
            this.validDate = formater.parse(validDate);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public Date getValidDate() {
        return validDate;
    }

    public String getValidDateAsString() {
        return formater.format(validDate);
    }

    
    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }

    public String getInsuranceNum() {
        return insuranceNum;
    }

    public void setInsuranceNum(String insuranceNum) {
        this.insuranceNum = insuranceNum;
    }

}
