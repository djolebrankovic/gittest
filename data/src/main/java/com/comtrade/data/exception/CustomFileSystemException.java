package com.comtrade.data.exception;

import org.apache.commons.vfs2.FileSystemException;

public class CustomFileSystemException extends FileSystemException {

    private static final long serialVersionUID = 1L;

    public CustomFileSystemException(String message) {
        super(message);
    }

    public CustomFileSystemException(FileSystemException exception) {
        super(exception);
    }

    public CustomFileSystemException(String message, FileSystemException exception) {
        super(message, exception);
    }
}
