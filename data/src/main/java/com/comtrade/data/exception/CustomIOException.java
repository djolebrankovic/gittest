package com.comtrade.data.exception;

import java.io.IOException;

public class CustomIOException extends IOException {

    private static final long serialVersionUID = 1L;

    public CustomIOException() {

    }

    public CustomIOException(String message) {
        super(message);
    }

    public CustomIOException(IOException e) {
        super(e);
    }

    public CustomIOException(String message, IOException e) {
        super(message, e);
    }
}