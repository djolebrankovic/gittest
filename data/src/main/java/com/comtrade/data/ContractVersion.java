package com.comtrade.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ContractVersion {

    private SimpleDateFormat formater = new SimpleDateFormat("dd.MM.yyyy");
    
    private String version;
    private Insurance insurance;
    private Date validDate;

    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    public Date getValidDate() {
        return validDate;
    }

    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }
    
    public void setValidDate(String validDate) {
        try {
            this.validDate = formater.parse(validDate);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
