package com.comtrade.data;

import java.util.ArrayList;

public class Contract {

	private String name;
	private int contractNum;
	private ArrayList<ContractVersion> contractVersions;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getContractNum() {
		return contractNum;
	}

	public void setContractNum(int contractNum) {
		this.contractNum = contractNum;
	}

	public ArrayList<ContractVersion> getContractVersions() {
		return contractVersions;
	}

	public void setContractVersions(ArrayList<ContractVersion> contractVersions) {
		this.contractVersions = contractVersions;
	}

	public ArrayList<Insurance> getValidInsurance() {
		ArrayList<Insurance> insurances = new ArrayList<Insurance>();

		if (contractVersions != null)
			for (ContractVersion conVersion : contractVersions) {
				if (conVersion.getValidDate().equals(conVersion.getInsurance().getValidDate()))
					insurances.add(conVersion.getInsurance());
			}

		return insurances;
	}

}
