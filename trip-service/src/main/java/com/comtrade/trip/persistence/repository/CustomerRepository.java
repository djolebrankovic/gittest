package com.comtrade.trip.persistence.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.comtrade.trip.model.Person;

@EnableMongoRepositories
public interface CustomerRepository extends MongoRepository<Person, String> {

}