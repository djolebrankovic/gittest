package com.comtrade.trip.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.comtrade.trip.rest.services.impl.PersonServiceImpl;

@Component
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(PersonServiceImpl.class);
	}

}