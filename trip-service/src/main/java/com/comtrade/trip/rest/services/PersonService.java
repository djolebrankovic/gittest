package com.comtrade.trip.rest.services;

import java.util.List;

import com.comtrade.trip.model.Person;

public interface PersonService {

	List<Person> getAllPersons();

	String addPerson(String personName);

}
