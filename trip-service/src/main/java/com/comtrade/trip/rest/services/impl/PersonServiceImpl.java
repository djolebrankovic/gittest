package com.comtrade.trip.rest.services.impl;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.comtrade.trip.model.Person;
import com.comtrade.trip.persistence.repository.CustomerRepository;

@Component
@Path("/person")
public class PersonServiceImpl {

	@Autowired
	CustomerRepository customerRepository;

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPersons() {
		return Response.status(Status.OK).entity(customerRepository.findAll()).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("add/{personName}")
	public Response addPerson(@PathParam("personName") String personName) {

		if (personName != null) {

			Person person = new Person();
			person.setName(personName);

			customerRepository.insert(person);

			return Response.status(javax.ws.rs.core.Response.Status.OK).entity(person).build();
		} else {
			return Response.status(javax.ws.rs.core.Response.Status.BAD_REQUEST).build();
		}
	}

}
