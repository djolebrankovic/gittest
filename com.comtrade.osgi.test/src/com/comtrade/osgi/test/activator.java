package com.comtrade.osgi.test;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

public class activator implements BundleActivator, ServiceListener {

    private DictionaryService service;
    private ServiceTracker<?, ?> dictionaryServiceTracker;
    private BundleContext fContext;
    private Dictionary dict;

    /*
     * (non-Javadoc)
     * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
     */
    @SuppressWarnings( "unchecked" )
    @Override
    public void start( final BundleContext context ) throws Exception {

        System.out.println( "Start" );
        fContext = context;
        dict = new DictionaryImpl();
        service = new DictionaryServiceImpl();
        service.registerDictionary( dict );

        // register the service
        context.registerService( DictionaryService.class.getName(), service, null );

        // create a tracker and track the service
        dictionaryServiceTracker = new ServiceTracker( context, DictionaryService.class.getName(), null );
        dictionaryServiceTracker.open();

        // have a service listener to implement the whiteboard pattern
        fContext.addServiceListener( this, "(objectclass=" + Dictionary.class.getName() + ")" );

        // grab the service
        service = (DictionaryService)dictionaryServiceTracker.getService();
    }

    /*
     * (non-Javadoc)
     * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop( final BundleContext context ) throws Exception {

        System.out.println( "Stop" );
        // close the service tracker
        dictionaryServiceTracker.close();
        dictionaryServiceTracker = null;

        service = null;
        fContext = null;
    }

    @Override
    public void serviceChanged( final ServiceEvent ev ) {
        final ServiceReference<?> sr = ev.getServiceReference();
        switch( ev.getType() ) {
            case ServiceEvent.REGISTERED: {
                final Dictionary dictionary = (Dictionary)fContext.getService( sr );
                service.registerDictionary( dictionary );
            }
                break;
            case ServiceEvent.UNREGISTERING: {
                final Dictionary dictionary = (Dictionary)fContext.getService( sr );
                service.unregisterDictionary( dictionary );
            }
                break;
            default:
                break;
        }
    }

}
