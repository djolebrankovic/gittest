package com.comtrade.eletricity.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "measurement")
public class Measurement {

	@Id
	@Column(name = "ID")
	private int id;

	@Column(name = "DATA")
	private int data;

	@Column(name = "MEASUREMENT_DATE")
	private Date measurementDate;

}
