package com.comtrade.eletricity.data;

import java.time.LocalDate;

public class MonthlyData {

	private static final float PRICE_KW = 5.9f;
	private static final float RTW_BILL = 500f;

	private String billNumber;
	private String billPeriod;
	private LocalDate dateOfIssue;
	private String placeOfIssue;
	private long lastKW;
	private long currentKW;

	public MonthlyData(String billNumber, String billPeriod, LocalDate dateOfIssue, String placeOfIssue, long lastKW,
			long currentKW) {
		this.billNumber = billNumber;
		this.billPeriod = billPeriod;
		this.dateOfIssue = dateOfIssue;
		this.placeOfIssue = placeOfIssue;
		this.lastKW = lastKW;
		this.currentKW = currentKW;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public String getBillPeriod() {
		return billPeriod;
	}

	public void setBillPeriod(String billPeriod) {
		this.billPeriod = billPeriod;
	}

	public LocalDate getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(LocalDate dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public String getPlaceOfIssue() {
		return placeOfIssue;
	}

	public void setPlaceOfIssue(String placeOfIssue) {
		this.placeOfIssue = placeOfIssue;
	}

	public float getCurrentBill() {
		return PRICE_KW * currentKW;
	}

	public float getLastBill() {
		return lastKW * PRICE_KW;
	}

	public float getRTWBIll() {
		return RTW_BILL;
	}

	public long getCurrentKW() {
		return currentKW;
	}

	public void setCurrentKW(long currentKW) {
		this.currentKW = currentKW;
	}

	public long getLastKW() {
		return lastKW;
	}

	public void setLastKW(long lastKW) {
		this.lastKW = lastKW;
	}

}
