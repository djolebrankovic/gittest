package com.comtrade.eletricity.data;

import java.util.List;

import com.comtrade.eletricity.model.Measurements;

public class ElectricityData {

	private MonthlyData monthlyData;
	private List<Measurements> yearlyData;
	private User user;

	public MonthlyData getMonthlyData() {
		return monthlyData;
	}

	public void setMonthlyData(MonthlyData monthlyData) {
		this.monthlyData = monthlyData;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Measurements> getYearlyData() {
		return yearlyData;
	}

	public void setYearlyData(List<Measurements> list) {
		this.yearlyData = list;
	}

}
