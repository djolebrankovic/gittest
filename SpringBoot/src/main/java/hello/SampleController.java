package hello;

import org.springframework.boot.SpringApplication;

import controller.HelloControler;
import controller.SecondHelloControler;

public class SampleController {

	public static void main(String[] args) throws Exception {

		Object[] services = { HelloControler.class, SecondHelloControler.class };

		SpringApplication.run(services, args);
	}
}
