package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SecondHelloControler {

	@RequestMapping("/second/hello")
	@ResponseBody
	String home() {
		return "Second Hello World!";
	}
	
}
